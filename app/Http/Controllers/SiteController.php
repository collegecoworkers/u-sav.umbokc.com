<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Org,
	Cnc,
	User
};

class SiteController extends Controller
{

	function __construct(){
		$this->middleware('auth');
	}

	function Index() {
		$orgs = Org::getsBy('user_id', User::id());
		return view('index')->with([
			'org_count' => Org::count(),
			'users_count' => User::count(),
			'cnc_count' => Cnc::count(),
			'cnc_count_a' => count(Cnc::getsBy('state', 'on')),
			'orgs' => $orgs,
		]);
	}

}
