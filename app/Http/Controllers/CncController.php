<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Org,
	Cnc,
	User
};

class CncController extends Controller {

	function __construct(){
		$this->middleware('auth');
	}

	function On($id) {
		$item = Cnc::getBy('id', $id);
		return view('cnc.on')->with([
			'model' => $item,
		]);
	}
	function Off($id) {
		$item = Cnc::getBy('id', $id);
		$item->procces = 0;
		$item->state = 'off';
		$item->save();
		return redirect()->back();
	}
	function View($id) {
		$item = Cnc::getBy('id', $id);
		return view('cnc.view')->with([
			'item' => $item,
		]);
	}
	function All($org_id) {
		$org = Org::getById($org_id);
		$items = Cnc::where('org_id', $org->id)->orderBy('id', 'desc')->get();

		return view('cnc.all')->with([
			'items' => $items,
			'org' => $org,
		]);
	}
	function Add($org_id) {
		return view('cnc.add')->with([
			'org_id' => $org_id,
		]);
	}
	function Edit($id) {
		$cnc = Cnc::getById($id);
		return view('cnc.edit')->with([
			'model' => $cnc,
		]);
	}
	function Create($org_id, Request $request) {
		$model = new Cnc();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->class = request()->class;
		$model->type = request()->type;
		$model->max_diam_item = request()->max_diam_item;
		$model->max_diam_drill = request()->max_diam_drill;
		$model->diam_shaft = request()->diam_shaft;
		$model->barrel_width = request()->barrel_width;
		$model->tool_magazine = request()->tool_magazine != null and request()->tool_magazine == 'on' ? 1 : 0 ;
		$model->auto_load = request()->auto_load != null and request()->auto_load == 'on' ? 1 : 0 ;
		$model->width = request()->width;
		$model->height = request()->height;
		$model->depth = request()->depth;
		$model->weight = request()->weight;
		$model->control_coord = request()->control_coord;
		$model->along_control_coord = request()->along_control_coord;
		$model->start_process = F::getDate();

		$model->org_id = $org_id;
		$model->setImg();

		$model->save();
		return redirect('/cncs/' . $org_id);
	}
	function Update($id, Request $request) {
		$model = Cnc::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->class = request()->class;
		$model->type = request()->type;
		$model->max_diam_item = request()->max_diam_item;
		$model->max_diam_drill = request()->max_diam_drill;
		$model->diam_shaft = request()->diam_shaft;
		$model->barrel_width = request()->barrel_width;
		$model->tool_magazine = request()->tool_magazine != null and request()->tool_magazine == 'on' ? 1 : 0 ;
		$model->auto_load = request()->auto_load != null and request()->auto_load == 'on' ? 1 : 0 ;
		$model->width = request()->width;
		$model->height = request()->height;
		$model->depth = request()->depth;
		$model->weight = request()->weight;
		$model->control_coord = request()->control_coord;
		$model->along_control_coord = request()->along_control_coord;

		$model->setImg();

		$model->save();
		return redirect('/cncs/' . $model->org_id);
	}
	function Enable($id, Request $request) {
		$model = Cnc::getById($id);

		$model->state = 'on';
		$model->start_process = F::getDate();

		$model->save();
		return redirect('/cncs/' . $model->org_id);
	}
	function Delete($id) {
		$org_id = Cnc::getBy('id', $id)->org_id;
		Cnc::where('id', $id)->delete();
		return redirect('/cncs/' . $org_id);
	}
}
