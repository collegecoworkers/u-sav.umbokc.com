<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Org,
	Cnc,
	User
};

class OrgController extends Controller {

	function __construct(){
		$this->middleware('auth');
	}

	function All() {
		$items = Org::getsBy('user_id', User::id());
		return view('org.all')->with([
			'items' => $items,
		]);
	}
	function Add() {
		return view('org.add');
	}
	function Edit($id) {
		$org = Org::getById($id);
		return view('org.edit')->with([
			'model' => $org,
		]);
	}
	function Create(Request $request) {
		$model = new Org();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->user_id = User::id();

		$model->save();
		return redirect('/');
	}
	function Update($id, Request $request) {
		$model = Org::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/');
	}
	function Delete($id) {
		Org::where('id', $id)->delete();
		return redirect('/');
	}
}
