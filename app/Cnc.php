<?php
namespace App;

class Cnc extends MyModel{

	private $diff_time = 5;
	private $procces_times = 4;
	private $efficiecy_times = 2;
	private $wear_times = 1;
	public $is_done = false;

	function calculate(){
		if ($this->wear == 0) return;
		$is_off = $this->state == 'off';

		$now_date = F::getDate();
		$now = date_create($now_date);
		$last = date_create($this->start_process);
		$time = F::formatDate(F::formatAllDate($last));
		$diff = date_diff($last, $now);

		$diff_s = F::getSeconds($diff);
		if($diff_s >= $this->diff_time){
			$times = floor($diff_s / $this->diff_time);
			if(!$is_off){
				F::incrField($this, 'procces', $this->procces_times * $times);
				F::incrField($this, 'efficiecy', -$this->efficiecy_times * $times);
				F::incrField($this, 'wear', -$this->wear_times * $times);
				if ($this->procces == 100) {
					$this->is_done = true;
					$this->procces = 0;
					$this->state = 'off';
				}
			} else if($this) {
				F::incrField($this, 'efficiecy', $this->efficiecy_times * $times);
			}
			$this->start_process = $now_date;
			$this->save();
		}
	}

	function getCurrClass() { return self::getClassOf($this->role);}
	static function getAllClasses(){ return [ 'А' => 'А', 'П' => 'П', 'В' => 'В', 'С' => 'С', ]; }
	static function getClassOf($r){ $def = 'A'; $roles = self::getAllClasses(); if(array_key_exists($r, $roles))  return $roles[$r]; return $roles[$def]; }

	function getType() { return self::getTypeOf($this->role);}
	static function getTypes(){ return [ 'F1' => 'F1', 'F2' => 'F2', 'F3' => 'F3', 'F4' => 'F4', 'C' => 'C', ]; }
	static function getTypeOf($r){ $def = 'F1'; $roles = self::getTypes(); if(array_key_exists($r, $roles))  return $roles[$r]; return $roles[$def]; }

	public function getImage() {
		return ($this->image != '') ?
		'/images/' . $this->image :
		'http://placehold.it/326x220';
	}

	function setImg() {
		if(request()->image == '' || !isset(request()->image)){
			$this->image = '';
			return;
		}
		request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);
		$imageName = time().'.'.request()->image->getClientOriginalExtension();
		request()->image->move(public_path('images'), $imageName);
		$this->image = $imageName;
	}

	static function attr() {
		return [
			'title' => 'Наиманование',
			'desc' => 'Описание',
			'image' => 'Фото',
			'class' => 'Класc точности',
			'type' => 'Вид',
			'max_diam_item' => 'Наибольший диаметр обробатываемого изделия',
			'max_diam_drill' => 'Наибольший диаметр сверления',
			'diam_shaft' => 'Диаметр расточного шпинделя',
			'barrel_width' => 'Ширна стола',
			'tool_magazine' => 'Наличие инструментального магазина',
			'auto_load' => 'Наличие устройства автоматичской загрзки заготовок',
			'width' => 'Ширина',
			'height' => 'Высота',
			'depth' => 'Глубина',
			'weight' => 'Масса',
			'control_coord' => 'Число управляемых координат',
			'along_control_coord' => 'Число одновременно управляемых координат',

			'state' => 'Состояние',

			'efficiecy' => 'КПД',
			'wear' => 'Износ',
			'procces' => 'Готовность изделия',

			'org_id' => 'Организация',
		];
	}
}
