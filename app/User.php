<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [ 'name', 'email', 'password' ];

	protected $hidden = [ 'password', 'remember_token' ];

  static function id() { return self::curr()->id; }
	static function curr() { return auth()->user(); }

	static function allArr($field = 'name'){ return F::toArr(self::all(), $field, 'id'); }

	static function getById($val){ return self::getBy('id', $val); }
	static function getBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->first(); else return self::queryBy([$col => $val])->first(); }
	static function getsBy($col, $val = null){ if(is_array($col)) return self::queryBy($col)->get(); else return self::queryBy([$col => $val])->get(); }
	static function queryBy($arr){ return self::where($arr); }
}
