<?php
namespace App;

class F {

	static function incrField(&$item, $field, $val){
		$tmp = intval($item->{$field});
		$tmp += $val;
		if ($tmp > 100) $tmp = 100;
		if ($tmp < 0) $tmp = 0;
		$item->{$field} = $tmp;
	}

	static function toArr($items, $val = 'title', $key = 'id'){
		$items_arr = [];
		foreach ($items as $item){
			if(is_callable($val)){
				$items_arr[$item->{$key}] = $val($item);
			} else {
				$items_arr[$item->{$key}] = $item->{$val};
			}
		}
		return $items_arr;
	}

	static function getDate(){
		return date("Y-m-d H:i:s");
	}

	static function toSeconds($diff){
		$diff_d = $diff->d;
		$diff_h = $diff->h + ($diff_d * 24);
		$diff_min = $diff->i + ($diff_h * 60);
		$diff_s = $diff->s + ($diff_min * 60);
		return $diff_s;
	}

	static function getSeconds($diff){
		$diff_d = $diff->d;
		$diff_h = $diff->h + ($diff_d * 24);
		$diff_min = $diff->i + ($diff_h * 60);
		$diff_s = $diff->s + ($diff_min * 60);
		return $diff_s;
	}

	static function formatDate($date){
		return date("H:i", strtotime($date));
	}

	static function formatAllDate($date){
		if(is_object($date))
			return $date->format("Y-m-d H:i:s");
		else
			return date("Y-m-d H:i:s", strtotime($date));
	}
}
