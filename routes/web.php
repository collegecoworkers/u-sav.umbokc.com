<?php

Auth::routes();

Route::get('/', 'SiteController@Index');

Route::get('/orgs', 'OrgController@All');
Route::get('/org/add', 'OrgController@Add');
Route::get('/org/edit/{id}', 'OrgController@Edit');
Route::get('/org/delete/{id}', 'OrgController@Delete');
Route::post('/org/create', 'OrgController@Create');
Route::post('/org/update/{id}', 'OrgController@Update');

Route::get('/on/{id}', 'CncController@On');
Route::get('/off/{id}', 'CncController@Off');
Route::post('/cnc/enable/{id}', 'CncController@Enable');

Route::get('/cncs/{org_id}', 'CncController@All');
Route::get('/cnc/view/{id}', 'CncController@View');
Route::get('/cnc/add/{org_id}', 'CncController@Add');
Route::get('/cnc/edit/{id}', 'CncController@Edit');
Route::get('/cnc/delete/{id}', 'CncController@Delete');
Route::post('/cnc/create/{org_id}', 'CncController@Create');
Route::post('/cnc/update/{id}', 'CncController@Update');
