@extends('layout.app')
@section('content')

<div class="row mtbox">
	<div class="col-md-3 col-sm-3  box0">
		<div class="box1">
			<span class="li_data"></span>
			<h3>{{ $org_count }}</h3>
		</div>
		<p>Заргистрированных организаций на нашем сервисе</p>
	</div>
	<div class="col-md-3 col-sm-3 box0">
		<div class="box1">
			<span class="li_user"></span>
			<h3>{{ $users_count }}</h3>
		</div>
		<p>Заргистрированных пользователей на нашем сервисе</p>
	</div>
	<div class="col-md-3 col-sm-3 box0">
		<div class="box1">
			<span class="li_stack"></span>
			<h3>{{ $cnc_count }}</h3>
		</div>
		<p>Созданых ЧПУ станков</p>
	</div>
	<div class="col-md-3 col-sm-3 box0">
		<div class="box1">
			<span class="li_stack"></span>
			<h3>{{ $cnc_count_a }}</h3>
		</div>
		<p>Работающих ЧПУ станков</p>
	</div>
</div>

<div class="row mtbox" style="margin-top: -20px;">
	<div class="col-lg-12">
		<h4><i class="fa fa-angle-right"></i> Список ваших организации</h4>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>Наименование</th>
					<th>Описание</th>
					<th>Действия</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($orgs as $item)
					<tr>
						<td>{{ $item->title }}</td>
						<td>{{ $item->desc }}</td>
						<td>
							<a href="/cncs/{{ $item->id }}"><i class="fa fa-eye"></i></a>
							<a href="/org/edit/{{ $item->id }}"><i class="fa fa-pencil"></i></a>
							<a onclick="return confirm('Вы уверенны?')" href="/org/delete/{{ $item->id }}"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
