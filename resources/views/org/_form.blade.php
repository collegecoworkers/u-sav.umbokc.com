{!! Form::open(['url' => isset($model) ? '/org/update/'.$model->id : '/org/create', 'class' => 'form-horizontal style-form']) !!}

	<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Наименование</label>
		<div class="col-sm-10"><input name="title" value="{{ isset($model) ? $model->title : ''}}" type="text" class="form-control" required></div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 col-sm-2 control-label">Описание</label>
		<div class="col-sm-10"><textarea name="desc" class="form-control" required rows="8">{{ isset($model) ? $model->desc : ''}}</textarea></div>
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}
