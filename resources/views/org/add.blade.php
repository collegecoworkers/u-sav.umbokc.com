@extends('layout.app')
@section('content')

<h3><i class="fa fa-angle-right"></i> Создание организации</h3>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			@include('org._form')
		</div>
	</div>
</div>
@endsection
