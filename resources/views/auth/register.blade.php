@extends('layout.auth')
@section('content')


<form class="form-login" method="post" action="{{ route('register') }}">
	{{ csrf_field() }}
	<h2 class="form-login-heading">Вход</h2>
	<div class="login-wrap">
		<br>
		<input class="form-control" name="full_name" placeholder="Ваше полное имя" type="text" />
		@if ($errors->has('full_name')) <span class="help-block"><strong>{{ $errors->first('full_name') }}</strong></span> @endif
		<br>
		<input class="form-control" name="name" placeholder="Логин" type="text" />
		@if ($errors->has('name')) <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span> @endif
		<br>
		<input class="form-control" name="email" placeholder="Email" type="text" />
		@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
		<br>
		<input class="form-control" name="password" placeholder="Пароль" type="password" />
		@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif
		<br>
		<input class="form-control" name="password_confirmation" placeholder="Повторите пароль" type="password" />
		@if ($errors->has('password_confirmation')) <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span> @endif
		<br>
		<button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> Зарегистрироваться</button>
		<hr>

		<div class="registration">
			<a class="" href="/login">
				Вход
			</a>
		</div>

	</div>

</form>     


@endsection
