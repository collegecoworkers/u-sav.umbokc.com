@extends('layout.auth')
@section('content')


<form class="form-login" method="post" action="{{ route('login') }}">
	{{ csrf_field() }}
	<h2 class="form-login-heading">Вход</h2>
	<div class="login-wrap">
		<br>

		<input class="form-control" name="email" placeholder="Email" type="text" />
		@if ($errors->has('email')) <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span> @endif
		<br>
		<input class="form-control" name="password" placeholder="Пароль" type="password" />
		@if ($errors->has('password')) <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span> @endif

		<br>
		<button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> Войти</button>
		<hr>

		<div class="registration">
			<a class="" href="/register">
				Создать аккаунт
			</a>
		</div>

	</div>

</form>     


@endsection
