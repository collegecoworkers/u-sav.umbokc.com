@extends('layout.app')
@section('content')
<style ea-s='d:n'>
.the-item { min-height: 220px; }
.the-item .t-prog{ margin: 0 5px 3px; }
.the-item .fr{ margin-top: 10px; }
.the-item .t-prog p{ margin: 0px; }
.the-item .t-prog .progress{ margin: 0px; }
.spotify .sp-title { top: 150px; }
.spotify { position: relative; }
.spotify .bg-over{  width: 100%;
  height: 220px;
  background: rgba(0, 0, 0, 0.33);
  position: absolute;
 }
</style>
<div class="row" ea-s='m:t m:l'>
	<h4 ea-s='m:l'>Список ЧПУ организации: {{ $org->title }}</h4>
	<div class="col-lg-12" ea-s='m:t'>
		<a href="/cnc/add/{{ $org->id }}" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</a>
		
		<div class="row" ea-s='m:t:big'>
			@foreach ($items as $item)
			@php
				$item->calculate();
			@endphp
			<div class="col-lg-4 col-md-4 col-sm-4 mb">
				<div class="content-panel the-item" >
					<div class="spotify" style="background-image: url({{ $item->getImage() }});">
						<div class="bg-over"></div>
						<div class="col-xs-4 col-xs-offset-8">
							<div class="btn-clear-g">
								@if ($item->wear != 0 && $item->efficiecy != 0)
									@if ($item->state == 'on')
										<a ea-s='c#f' class="mmmm btn btn-sm" href="/off/{{ $item->id }}"><i class="fa fa-stop"></i></a>
									@else
										<a ea-s='c#f' class="mmmm btn btn-sm" href="/on/{{ $item->id }}"><i class="fa fa-play"></i></a>
									@endif
								@endif
								<a ea-s='c#f' class="mmmm btn btn-sm" href="/cnc/view/{{ $item->id }}"><i class="fa fa-eye"></i></a>
								<a ea-s='c#f' class="mmmm btn btn-sm" href="/cnc/edit/{{ $item->id }}"><i class="fa fa-pencil"></i></a>
								<a ea-s='c#f' class="mmmm btn btn-sm" onclick="return confirm('Вы уверенны?')" href="/cnc/delete/{{ $item->id }}"><i class="fa fa-trash-o"></i></a>
							</div>
						</div>
						<div class="sp-title">
							<h3>{{ $item->title }}</h3>
						</div>
					</div>
					<div class="fr"></div>
					@if ($item->state == 'on')
						<script>
							window.cnc_has_item = true;
						</script>
						<div class="t-prog">
							<p>Готовность изделия {{ $item->procces }}%</p>
							<div class="progress progress-striped active">
								<div class="progress-bar" role="progressbar" aria-valuenow="{{ $item->procces }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $item->procces }}%">
									<span class="sr-only">{{ $item->procces }}%</span>
								</div>
							</div>
						</div>
					@endif
					@if ($item->is_done)
						<script type="text/javascript">
							$(document).ready(function () {
								$.gritter.add({
											title: '{{ $item->title }}',
											text: 'Деталь готова',
											image: "{{ $item->getImage() }}",
											sticky: true,
											time: '',
											class_name: 'my-sticky-class'
										});
								return false;
							});
						</script>

					@endif
					<div class="t-prog">
						<p>КПД {{ $item->efficiecy }}%</p>
						<div class="progress progress-striped">
							<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="{{ $item->efficiecy }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $item->efficiecy }}%">
								<span class="sr-only">{{ $item->efficiecy }}%</span>
							</div>
						</div>
					</div>
					<div class="t-prog">
						<p>Износ {{ $item->wear }}%</p>
						<div class="progress progress-striped">
							<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="{{ $item->wear }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $item->wear }}%">
								<span class="sr-only">{{ $item->wear }}%</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
		</div>

	</div>
</div>
<script>
	if ('cnc_has_item' in window) {
		setTimeout('location.reload();', 5000);
	}
</script>
@endsection
