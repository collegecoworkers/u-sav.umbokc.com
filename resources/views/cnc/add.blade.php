@extends('layout.app')
@section('content')

<h3><i class="fa fa-angle-right"></i> Создать ЧПУ</h3>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
			@include('cnc._form')
		</div>
	</div>
</div>
@endsection
