@php
use App\Cnc;
use App\Org;
@endphp
@extends('layout.app')
@section('content')

<h3><i class="fa fa-angle-right"></i> Наименование ЧПУ: {{ $item->title }}</h3>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">
      <p>
        <b>{{ Cnc::attr()['title'] }}</b>: {{ $item->title }}
      </p>
      <p>
        <b>{{ Cnc::attr()['desc'] }}</b>: {{ $item->desc }}
      </p>
      <p>
        <b>{{ Cnc::attr()['class'] }}</b>: {{ $item->class }}
      </p>
      <p>
        <b>{{ Cnc::attr()['type'] }}</b>: {{ $item->type }}
      </p>
      <p>
        <b>{{ Cnc::attr()['max_diam_item'] }}</b>: {{ $item->max_diam_item }}
      </p>
      <p>
        <b>{{ Cnc::attr()['max_diam_drill'] }}</b>: {{ $item->max_diam_drill }}
      </p>
      <p>
        <b>{{ Cnc::attr()['diam_shaft'] }}</b>: {{ $item->diam_shaft }}
      </p>
      <p>
        <b>{{ Cnc::attr()['barrel_width'] }}</b>: {{ $item->barrel_width }}
      </p>
      <p>
        <b>{{ Cnc::attr()['tool_magazine'] }}</b>: {{ $item->tool_magazine == 1 ? 'Да' : 'Нет' }}
      </p>
      <p>
        <b>{{ Cnc::attr()['auto_load'] }}</b>: {{ $item->auto_load == 1 ? 'Да' : 'Нет' }}
      </p>
      <p>
        <b>{{ Cnc::attr()['width'] }}</b>: {{ $item->width }}
      </p>
      <p>
        <b>{{ Cnc::attr()['height'] }}</b>: {{ $item->height }}
      </p>
      <p>
        <b>{{ Cnc::attr()['depth'] }}</b>: {{ $item->depth }}
      </p>
      <p>
        <b>{{ Cnc::attr()['weight'] }}</b>: {{ $item->weight }}
      </p>
      <p>
        <b>{{ Cnc::attr()['control_coord'] }}</b>: {{ $item->control_coord }}
      </p>
      <p>
        <b>{{ Cnc::attr()['along_control_coord'] }}</b>: {{ $item->along_control_coord }}
      </p>
      <p>
        <b>{{ Cnc::attr()['org_id'] }}</b>: <a href="/cncs/{{ $item->org_id }}">{{ Org::getById($item->org_id)->title }}</a>
      </p>
      <p>
        <b>{{ Cnc::attr()['image'] }}</b>: <br> <img ea-j='w=326px h=220px' src="{{ $item->getImage() }}" alt="">
      </p>
    </div>
  </div>
</div>
@endsection
