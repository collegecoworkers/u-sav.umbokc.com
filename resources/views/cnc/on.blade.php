@extends('layout.app')
@section('content')

<h3><i class="fa fa-angle-right"></i> Включить ЧПУ: {{ $model->title }}</h3>
<div class="row mt">
	<div class="col-lg-12">
		<div class="form-panel">

			{!! Form::open(['enctype'=>'multipart/form-data', 'url' => '/cnc/enable/'.$model->id , 'class' => 'style-form']) !!}

			<div class="form-group">
				<label class="control-label">Загрузите 3D модель</label>
				{!! Form::file('image', ['required' => '','class' => 'form-control']) !!}
			</div>

			<div class="form-actions">
				<button type="submit" class="btn btn-success">Поехали</button>
			</div>
			{!! Form::close() !!}

		</div>
	</div>
</div>
@endsection
