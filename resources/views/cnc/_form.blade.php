@php
	use App\Cnc;
@endphp
{!! Form::open(['enctype'=>'multipart/form-data', 'url' => isset($model) ? '/cnc/update/'.$model->id : '/cnc/create/' . $org_id, 'class' => 'style-form']) !!}

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['title'] }}</label>
		<input name="title" value="{{ isset($model) ? $model->title : ''}}" type="text" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['desc'] }}</label>
		<textarea name="desc" class="form-control" required>{{ isset($model) ? $model->desc : ''}}</textarea>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['class'] }}</label>
		{!! Form::select('class', Cnc::getAllClasses(), isset($mode) ? $model->class : '', ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['type'] }}</label>
		{!! Form::select('type', Cnc::getTypes(), isset($mode) ? $model->type : '', ['class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['max_diam_item'] }}</label>
		<input name="max_diam_item" value="{{ isset($model) ? $model->max_diam_item : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['max_diam_drill'] }}</label>
		<input name="max_diam_drill" value="{{ isset($model) ? $model->max_diam_drill : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['diam_shaft'] }}</label>
		<input name="diam_shaft" value="{{ isset($model) ? $model->diam_shaft : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['barrel_width'] }}</label>
		<input name="barrel_width" value="{{ isset($model) ? $model->barrel_width : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['tool_magazine'] }}</label>
		<div class=" the-shitch switch switch-square" data-on-label="<i class=' fa fa-check'></i>" data-off-label="<i class='fa fa-times'></i>">
			<input type="checkbox" {{ isset($model) ? $model->tool_magazine == 1 ? 'checked' : '' : 'checked'}} name="tool_magazine" />
		</div>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['auto_load'] }}</label>
		<div class=" the-shitch switch switch-square" data-on-label="<i class=' fa fa-check'></i>" data-off-label="<i class='fa fa-times'></i>">
			<input type="checkbox" {{ isset($model) ? $model->auto_load == 1 ? 'checked' : '' : 'checked'}} name="auto_load" />
		</div>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['width'] }}</label>
		<input name="width" value="{{ isset($model) ? $model->width : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['height'] }}</label>
		<input name="height" value="{{ isset($model) ? $model->height : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['depth'] }}</label>
		<input name="depth" value="{{ isset($model) ? $model->depth : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['weight'] }}</label>
		<input name="weight" value="{{ isset($model) ? $model->weight : ''}}" type="number" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['control_coord'] }}</label>
		<input name="control_coord" value="{{ isset($model) ? $model->control_coord : ''}}" type="text" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['along_control_coord'] }}</label>
		<input name="along_control_coord" value="{{ isset($model) ? $model->along_control_coord : ''}}" type="text" class="form-control" required>
	</div>

	<div class="form-group">
		<label class="control-label">{{ Cnc::attr()['image'] }}</label>
		{!! Form::file('image', ['placeholder' => 'Фото','class' => 'form-control']) !!}
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}

<style ea-s='d:n'>
	.the-shitch{
		margin-left: 10px;
		margin-bottom: -10px;
	}
</style>
