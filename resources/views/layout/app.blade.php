<!doctype html>
<html lang="{{ app()->getLocale() }}" ea>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=2" rel="stylesheet">
	<link href="/assets/css/bootstrap.css" rel="stylesheet">
	<link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="/assets/js/gritter/css/jquery.gritter.css" rel="stylesheet"/>
	<link href="/assets/lineicons/style.css" rel="stylesheet">
	<link href="/assets/css/style.css" rel="stylesheet">
	<link href="/assets/css/style-responsive.css" rel="stylesheet">

	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=2"></script>
	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/jquery-1.8.3.min.js"></script>
	<script src="/assets/js/gritter/js/jquery.gritter.js"></script>
	<script src="/assets/js/gritter-conf.js"></script>

</head>
<body >
	<section id="container" >
		
		@include('layout.nav')

		@include('layout.sidebar')

		<section id="main-content">
			<section class="wrapper">
				<div class="row">
					<div class="col-lg-12">
						@yield('content')
					</div>
				</div>
			</section>
		</section>

		<footer class="site-footer">
			<div class="text-center">
				&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.
			</div>
		</footer>
	</section>

	<script src="/assets/js/bootstrap.min.js"></script>
	<script class="include" src="/assets/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="/assets/js/jquery.scrollTo.min.js"></script>
	<script src="/assets/js/jquery.nicescroll.js"></script>
	<script src="/assets/js/jquery.sparkline.js"></script>


	<script src="/assets/js/common-scripts.js"></script>
	<script src="/assets/js/bootstrap-switch.js"></script>
	<script src="/assets/js/jquery.tagsinput.js"></script>
	
	<script src="/assets/js/form-component.js"></script>

	<script type="application/javascript">
		$(document).ready(function () {
			$("#date-popover").popover({html: true, trigger: "manual"});
			$("#date-popover").hide();
			$("#date-popover").click(function (e) {
				$(this).hide();
			});
		});


		function myNavFunction(id) {
			$("#date-popover").hide();
			var nav = $("#" + id).data("navigation");
			var to = $("#" + id).data("to");
			console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
		}
	</script>
</body>
</html>
