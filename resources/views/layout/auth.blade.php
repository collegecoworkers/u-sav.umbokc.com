<!doctype html>
<html lang="{{ app()->getLocale() }}" ea>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=2" rel="stylesheet">
	<link href="/assets/css/bootstrap.css" rel="stylesheet">
	<link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="/assets/css/style.css" rel="stylesheet">
	<link href="/assets/css/style-responsive.css" rel="stylesheet">

</head>
<body >

	<div id="login-page">
		<div class="container">
			@yield('content')
		</div>
	</div>

	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>

	<script src="/assets/js/jquery.backstretch.min.js"></script>
	<script>
		$.backstretch("/assets/img/login-bg.jpg", {speed: 500});
	</script>

</body>
</html>
