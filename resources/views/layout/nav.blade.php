<header class="header black-bg">
	<div class="sidebar-toggle-box">
		<div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
	</div>
	<a href="/" class="logo"><b>{{ config('app.name', 'Laravel') }}</b></a>
	<div class="nav notify-row" id="top_menu">
	</div>
	<div class="top-menu">
		<ul class="nav pull-right top-menu">
			<li>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
				<a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
			</li>
		</ul>
	</div>
</header>
