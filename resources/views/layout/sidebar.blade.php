@php
use App\{
	Org,
	User
};

$curr = User::curr();
$c_url = Request::path();
if ($c_url != '/') {
	$c_url = '/'.$c_url;
}
$orgs = Org::getsBy('user_id', $curr->id);

$nav = [];

foreach ($orgs as $i) {

	$url = '/cncs/' . $i->id;

	$nav[] = [
		'active' => $c_url == $url,
		'url' => $url,
		'icon' => 'cogs',
		'label' => $i->title
	];
}

@endphp

<aside>
	<div id="sidebar"  class="nav-collapse ">
		<ul class="sidebar-menu" id="nav-accordion">

			<p class="centered"><a href="/"><img src="/assets/img/ui-sam.jpg" class="img-circle" width="60"></a></p>
			<h5 class="centered">{{ $curr->name }}</h5>

			<li class="mt">
				<a class="{{ $c_url == '/' ? 'active' : ''}}" href="/">
					<i class="fa fa-dashboard"></i>
					<span>Главная</span>
				</a>
			</li>

			<li>
				<span>Оргранизации</span>
			</li>
			
			@foreach ($nav as $item)
				<li class="mt">
					<a class="{{ $item['active'] ? 'active' : ''}}" href="{{ $item['url'] }}">
						<i class="fa fa-{{ $item['icon'] }}"></i>
						<span>{{ $item['label'] }}</span>
					</a>
				</li>
			@endforeach

			<li class="mt">
				<a class="" href="/org/add">
					<i class="fa fa-plus"></i>
					<span>Создать</span>
				</a>
			</li>

		</ul>
		<!-- sidebar menu end-->
	</div>
</aside>
