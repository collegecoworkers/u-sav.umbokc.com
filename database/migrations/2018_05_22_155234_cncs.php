<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cncs extends Migration
{

	public function up()
	{
		Schema::create('cncs', function (Blueprint $table) {
			$table->increments('id');

			$table->string('title');
			$table->text('desc');
			$table->string('image');
			$table->enum('class', ['А', 'П', 'В', 'С']);
			$table->enum('type', ['F1', 'F2', 'F3', 'F4', 'C']);
			$table->integer('max_diam_item')->default(0);
			$table->integer('max_diam_drill')->default(0);
			$table->integer('diam_shaft')->default(0);
			$table->integer('barrel_width')->default(0);
			$table->boolean('tool_magazine')->default(false);
			$table->boolean('auto_load')->default(false);
			$table->integer('width')->default(0);
			$table->integer('height')->default(0);
			$table->integer('depth')->default(0);
			$table->integer('weight')->default(0);
			$table->string('control_coord')->default(0);
			$table->string('along_control_coord')->default(0);

			$table->enum('state', ['off', 'on'])->default('off');

			$table->integer('efficiecy')->default(100);
			$table->integer('wear')->default(100);
			$table->integer('procces')->default(0);

			$table->integer('org_id');

			$table->rememberToken();
			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down()
	{
		//
	}
}
